# Bitbucket Pipelines Pipe: AWS SAM deploy

Deploy an AWS serverless lambda stack using [AWS Serverless Application Model (AWS SAM)](https://aws.amazon.com/serverless/sam/).


## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:
    
```yaml
- pipe: atlassian/aws-sam-deploy:0.5.2
  variables:
    AWS_ACCESS_KEY_ID: '<string>' # Optional if already defined in the context.
    AWS_SECRET_ACCESS_KEY: '<string>' # Optional if already defined in the context.
    AWS_DEFAULT_REGION: '<string>' # Optional if already defined in the context.
    S3_BUCKET: '<string>'
    STACK_NAME: '<string>'
    # CAPABILITIES: '<array>' # Optional.
    # SAM_TEMPLATE: '<string>' # Optional.
    # CFN_TEMPLATE: '<string>' # Optional.
    # TEMPLATE: '<string>' # Optional.
    # COMMAND: '<string>' # Optional.
    # STACK_PARAMETERS: '<json>' # Optional.
    # WAIT: '<boolean>' # Optional.
    # WAIT_INTERVAL: '<integer>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```


## Variables

### Basic usage

| Variable                        | Usage                                                |
| ------------------------------- | ---------------------------------------------------- |
| AWS_ACCESS_KEY_ID (**)           |  AWS access key. |
| AWS_SECRET_ACCESS_KEY (**)       |  AWS secret key. |
| AWS_DEFAULT_REGION (**)          |  The AWS region code (us-east-1, us-west-2, etc.) of the region containing the AWS resource(s). For more information, see [Regions and Endpoints][Regions and Endpoints] in the _Amazon Web Services General Reference_. |
| S3_BUCKET (*)                   |  The name of the AWS S3 bucket for artifacts. |
| STACK_NAME (*)                  |  The name of the AWS CloudFormation stack. If the stack does not exist, it will be created. |
| CAPABILITIES                    |  Array of [capabilities][capabilities]. Allowed values: `[CAPABILITY_IAM, CAPABILITY_NAMED_IAM, CAPABILITY_AUTO_EXPAND]`. |
| SAM_TEMPLATE                    |  Location of the file containing a SAM template body in your repository. Default: `template.yaml`. Valid template body formats are: `json` or `yaml`. See **Prerequisites**. |
| CFN_TEMPLATE                    |  Name of the CloudFormation template to generate. Default: `packaged.yml`. |
| TEMPLATE                        |  Location of the file containing a [packaged CloudFormation template body][packaged template body] in your repository or a URL that points to the template hosted in an Amazon S3 bucket. Default: `$BITBUCKET_PIPE_STORAGE_DIR/packaged.yml` stored locally after executing the pipe with the 'package-only' or 'all' command. Valid template body formats are: `json` or `yaml`. See **Prerequisites**. |
| COMMAND                         |  Command to be executed during the deployment. Valid options are `all`, `package-only`, `deploy-only`. Default: `all`. |
| STACK_PARAMETERS                |  JSON document containing variables to pass to a pipeline you want to trigger. The value should be a list of object with `ParameterKey`, `ParameterValue` fields for each of your variables. The **Examples** section below contains an example for passing parameters to the stack. |
| WAIT                            |  Wait for deployment to complete. Default: `false`. If false, it will finish when the cloudformation deploy is triggered and will not wait until the resources are successfully created. |
| WAIT_INTERVAL                   |  Time to wait between polling for deployment to complete (in seconds). Default: `30`. |
| DEBUG                           |  Turn on extra debug information. Default: `false`. |
_(*) = required variable. This variable needs to be specified always when using the pipe._
_(**) = required variable. If this variable is configured as a repository, account or environment variable, it doesn’t need to be declared in the pipe as it will be taken from the context. It can still be overridden when using the pipe._


### Advanced usage

If `COMMAND` is set to `package-only`

| Variable                        | Usage                                                |
| ------------------------------- | ---------------------------------------------------- |
| AWS_ACCESS_KEY_ID (**)           |  AWS access key. |
| AWS_SECRET_ACCESS_KEY (**)       |  AWS secret key. |
| AWS_DEFAULT_REGION (**)          |  The AWS region code (us-east-1, us-west-2, etc.) of the region containing the AWS resource(s). For more information, see [Regions and Endpoints][Regions and Endpoints] in the _Amazon Web Services General Reference_. |
| STACK_NAME (*)                  |  The name of the AWS CloudFormation stack. If the stack does not exist, it will be created. |
| S3_BUCKET (*)                   |  The name of the AWS S3 bucket for artifacts. |
| COMMAND (*)                     |  Command to be used. Use `package-only` here. |
| SAM_TEMPLATE                    |  Location of the file containing a SAM template body in your repository. Default: `template.yaml`. Valid template body formats are: `json` or `yaml`. See **Prerequisites**. |
| CFN_TEMPLATE                    |  Name of the CloudFormation template to generate. Default: `packaged.yml`. |
| STACK_PARAMETERS                |  JSON document containing variables to pass to a pipeline you want to trigger. The value should be a list of object with `ParameterKey`, `ParameterValue` fields for each of your variables. The **Examples** section below contains an example for passing parameters to the stack. |
| WAIT                            |  Wait for deployment to complete. Default: `false`. |
| WAIT_INTERVAL                   |  Time to wait between polling for deployment to complete (in seconds). Default: `30`. |
| DEBUG                           |  Turn on extra debug information. Default: `false`. |
_(*) = required variable. This variable needs to be specified always when using the pipe._
_(**) = required variable. If this variable is configured as a repository, account or environment variable, it doesn’t need to be declared in the pipe as it will be taken from the context. It can still be overridden when using the pipe._


If `COMMAND` is set to `deploy-only`

| Variable                        | Usage                                                |
| ------------------------------- | ---------------------------------------------------- |
| AWS_ACCESS_KEY_ID (**)           |  AWS access key. |
| AWS_SECRET_ACCESS_KEY (**)       |  AWS secret key. |
| AWS_DEFAULT_REGION (**)          |  The AWS region code (us-east-1, us-west-2, etc.) of the region containing the AWS resource(s). For more information, see [Regions and Endpoints][Regions and Endpoints] in the _Amazon Web Services General Reference_. |
| STACK_NAME (*)                  |  The name of the AWS CloudFormation stack. If the stack does not exist, it will be created. |
| CAPABILITIES                    |  Array of [capabilities][capabilities]. Allowed values: `[CAPABILITY_IAM, CAPABILITY_NAMED_IAM, CAPABILITY_AUTO_EXPAND]`. |
| COMMAND (*)                     |  Command to be used. Use `deploy-only` here. |
| TEMPLATE                        |  Location of the file containing a [packaged CloudFormation template body][packaged template body] in your repository or a URL that points to the template hosted in an Amazon S3 bucket. Default: `$BITBUCKET_PIPE_STORAGE_DIR/packaged.yml` stored locally after `package-only` command. Valid template body formats are: `json` or `yaml`. See **Prerequisites**. |
| STACK_PARAMETERS                |  JSON document containing variables to pass to a pipeline you want to trigger. The value should be a list of object with `ParameterKey`, `ParameterValue` fields for each of your variables. The **Examples** section below contains an example for passing parameters to the stack. |
| WAIT                            |  Wait for deployment to complete. Default: `false`. If `false` and template correctly deployed to AWS Cloud the pipe finished with success deploy status without waiting for all services inside to be deployed. |
| WAIT_INTERVAL                   |  Time to wait between polling for deployment to complete (in seconds). Default: `30`. |
| DEBUG                           |  Turn on extra debug information. Default: `false`. |
_(*) = required variable. This variable needs to be specified always when using the pipe._
_(**) = required variable. If this variable is configured as a repository, account or environment variable, it doesn’t need to be declared in the pipe as it will be taken from the context. It can still be overridden when using the pipe._


## Details

The AWS Serverless Application Model (SAM) is an open-source framework for building serverless applications. It provides shorthand syntax to express functions, APIs, databases, and event source mappings. With just a few lines per resource, you can define the application you want and model it using YAML. During deployment, SAM transforms and expands the SAM syntax into AWS CloudFormation syntax, enabling you to build serverless applications faster.


For advanced use cases and best practices, we recommend _build once and deploy many_ approach. So, if you have multiple environments we recommend using the `COMMAND` variable to separate your CI/CD workflow into different operations / pipes:

- `COMMAND: 'package-only'`: It will package an AWS SAM application. The command uploads local artifacts, such as source code for an AWS Lambda function, to an S3 bucket. The command creates and writes the packaged template to the `$BITBUCKET_PIPE_STORAGE_DIR/packaged.yml`.
- `COMMAND: 'deploy-only'`: It will deploy your configuration as code using [AWS CloudFormation](https://aws.amazon.com/cloudformation/).


## Prerequisites
* An IAM user is configured with sufficient permissions to perform a deployment of your application using CloudFormation.
* You have configured the AWS S3 bucket.
* CloudFormation templates greater than 51200 bytes must be deployed from an AWS S3 bucket by providing AWS S3 URL. For further details please see [AWS CloudFormation limits][AWS CloudFormation limits].


## Examples 

### Basic example:

Deploy a new version of your AWS SAM stack, using a template file located in your repository.

```yaml
script:
  - pipe: atlassian/aws-sam-deploy:0.5.2
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      S3_BUCKET: 'my-s3-bucket'
      STACK_NAME: 'my-stack-name'
```

Deploy a new version of your AWS SAM stack, using a template file located in your repository. `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` and `AWS_DEFAULT_REGION` are configured as repository variables, so there is no need to declare them in the pipe.

```yaml
script:
  - pipe: atlassian/aws-sam-deploy:0.5.2
    variables:
      S3_BUCKET: 'my-s3-bucket'
      STACK_NAME: 'my-stack-name'
```

### Advanced example:

Deploy a new version of your AWS SAM stack and wait until the deployment has completed.

```yaml
script:
  - pipe: atlassian/aws-sam-deploy:0.5.2
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      S3_BUCKET: 'my-s3-bucket'
      STACK_NAME: 'my-stack-name'
      SAM_TEMPLATE: 'sam_template.yaml'
      CAPABILITIES: ['CAPABILITY_IAM', 'CAPABILITY_AUTO_EXPAND']
      WAIT: 'true'
      WAIT_INTERVAL: 60
```

Deploy a new version of your AWS SAM stack with additional parameters for the stack.

```yaml
script:
  - pipe: atlassian/aws-sam-deploy:0.5.2
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      S3_BUCKET: 'my-s3-bucket'
      STACK_NAME: 'my-stack-name'
      SAM_TEMPLATE: 'sam_template.yaml'
      CAPABILITIES: ['CAPABILITY_IAM', 'CAPABILITY_AUTO_EXPAND']
      STACK_PARAMETERS: >
        [{
          "ParameterKey": "KeyName",
          "ParameterValue": "mykey"
        },
        {
          "ParameterKey": "DBUser",
          "ParameterValue": "mydbuser"
        },
        {
          "ParameterKey": "DBPassword",
          "ParameterValue": $DB_PASSWORD
        }]
```

Package an AWS SAM application, upload the generated stack template to an AWS S3 Bucket with [AWS S3 deploy pipe](https://bitbucket.org/atlassian/aws-s3-deploy) and then deploy a new version of your stack, using the template file located in the Amazon S3 bucket.

> At first, we use `COMMAND: 'package-only'`: It will package an AWS SAM application. The command uploads local artifacts, such as source code for an AWS Lambda function, to an S3 bucket. The command creates and writes the packaged template to the `$BITBUCKET_PIPE_STORAGE_DIR/packaged.yml`.
> The variable `$BITBUCKET_PIPE_STORAGE_DIR` stores the related path `.bitbucket/pipelines/generated/pipeline/pipes/atlassian/aws-sam-deploy`, see [Sharing information between pipes][Sharing information between pipes].
> The variable `$BITBUCKET_PIPE_STORAGE_DIR` available only inside the pipe.
> That's why for passing `packaged.yml` as a variable LOCAL_PATH for atlassian/aws-s3-deploy we should provide related path directly.

```yaml
script:
  - pipe: atlassian/aws-sam-deploy:0.5.2
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      S3_BUCKET: 'my-s3-bucket'
      STACK_NAME: 'my-stack-name'
      SAM_TEMPLATE: 'sam_template.yaml'
      CAPABILITIES: ['CAPABILITY_IAM', 'CAPABILITY_AUTO_EXPAND']
      COMMAND: 'package-only'

  - pipe: atlassian/aws-s3-deploy:0.3.2
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      S3_BUCKET: 'my-bucket-name'
      # template packaged.yml after pipe atlassian/aws-sam-deploy package-only execution
      LOCAL_PATH: '.bitbucket/pipelines/generated/pipeline/pipes/atlassian/aws-sam-deploy'
      EXTRA_ARGS: '--exclude=* --include=packaged.yml'

  - pipe: atlassian/aws-sam-deploy:0.5.2
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      STACK_NAME: 'my-stack-name'
      TEMPLATE: 'https://s3.amazonaws.com/my-bucket-name/packaged.yml'
      CAPABILITIES: ['CAPABILITY_IAM', 'CAPABILITY_AUTO_EXPAND']
      COMMAND: 'deploy-only'
```

How to use template variables:

If you want to create a package use the variable `SAM_TEMPLATE` and the `COMMAND: 'package-only'`:

> `COMMAND: 'package-only'`will package an AWS SAM application; uploads local artifacts, such as source code for an AWS Lambda function, to an S3 bucket; creates and writes the packaged template to the `$BITBUCKET_PIPE_STORAGE_DIR/packaged.yml`.

```yaml
script:
  - pipe: atlassian/aws-sam-deploy:0.5.2
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      S3_BUCKET: 'my-s3-bucket'
      STACK_NAME: 'my-stack-name'
      SAM_TEMPLATE: 'sam_template.yaml'
      CAPABILITIES: ['CAPABILITY_IAM', 'CAPABILITY_AUTO_EXPAND']
      COMMAND: 'package-only'
```

You can change the default name of the packaged.yml if you provide a variable `CFN_TEMPLATE`:

> `COMMAND: 'package-only'`will package an AWS SAM application; uploads local artifacts, such as source code for an AWS Lambda function, to an S3 bucket; creates and writes the packaged template to the `$BITBUCKET_PIPE_STORAGE_DIR/$CFN_TEMPLATE`.

```yaml
script:
  - pipe: atlassian/aws-sam-deploy:0.5.2
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      S3_BUCKET: 'my-s3-bucket'
      STACK_NAME: 'my-stack-name'
      SAM_TEMPLATE: 'sam_template.yaml'
      CFN_TEMPLATE: 'my-custom-name-packaged.yml'
      CAPABILITIES: ['CAPABILITY_IAM', 'CAPABILITY_AUTO_EXPAND']
      COMMAND: 'package-only'
```

If you have an AWS CoudFormation template and want to deploy it, use variable `TEMPLATE` and `COMMAND: 'deploy-only'`:

> Template can be located in your repository or hosted in an Amazon S3 bucket.

```yaml
  - pipe: atlassian/aws-sam-deploy:0.5.2
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      STACK_NAME: 'my-stack-name'
      TEMPLATE: 'https://s3.amazonaws.com/my-bucket-name/packaged.yml'
      CAPABILITIES: ['CAPABILITY_IAM', 'CAPABILITY_AUTO_EXPAND']
      COMMAND: 'deploy-only'
```


## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce


## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,aws,serverless
[capabilities]: https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/using-iam-template.html#using-iam-capabilities
[AWS CloudFormation limits]: https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/cloudformation-limits.html
[packaged template body]: https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/sam-cli-command-reference-sam-package.html
[Regions and Endpoints]: https://docs.aws.amazon.com/general/latest/gr/rande.html
[Sharing information between pipes]: https://confluence.atlassian.com/bitbucket/advanced-techniques-for-writing-pipes-969511009.html#Advancedtechniquesforwritingpipes-Sharinginformationbetweenpipes