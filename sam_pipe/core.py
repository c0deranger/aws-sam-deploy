import os
import sys
import subprocess

import yaml
from bitbucket_pipes_toolkit import get_logger

from pipe import main as cfn_pipe


MSG_AWS_SAM_PACKAGE_FAILED = 'Failed SAM package.'
DEFAULT_SAM_TEMPLATE_FILENAME = 'template.yaml'
DEFAULT_CFN_TEMPLATE_FILENAME = 'packaged.yml'


schema_commands = ["all", "package-only", "deploy-only"]

common_schema = {
    "AWS_ACCESS_KEY_ID": {
        "type": "string",
        "required": True
    },
    "AWS_SECRET_ACCESS_KEY": {
        "type": "string",
        "required": True
    },
    "AWS_DEFAULT_REGION": {
        "type": "string",
        "required": True
    },
    "COMMAND": {
        "type": "string",
        "allowed": schema_commands,
        "default": schema_commands[0]
    },
}

cloudformation_schema = cfn_pipe.schema
package_schema = {
    "S3_BUCKET": {
        "type": "string",
        "required": True
    },
    "SAM_TEMPLATE": {
        "type": "string",
        "default": DEFAULT_SAM_TEMPLATE_FILENAME
    },
    "CFN_TEMPLATE": {
        "type": "string",
        "default": DEFAULT_CFN_TEMPLATE_FILENAME
    }
}
package_schema.update(common_schema)

logger = get_logger()


class SamDeployPipe(cfn_pipe.CloudformationDeployPipe):

    def package(self):
        """
        Packages the local artifacts (local paths) that your AWS SAM template references.
        The command uploads local artifacts, such as source code for AWS Lambda functions, to an S3 bucket.
        https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/sam-cli-command-reference-sam-package.html
        :return:
        """
        logger.info(f'Start package...')

        s3_bucket = self.get_variable('S3_BUCKET')
        sam_template_file = self.get_variable('SAM_TEMPLATE')
        cloudformation_template_file = os.path.join(os.getenv('BITBUCKET_PIPE_STORAGE_DIR'), self.get_variable('CFN_TEMPLATE'))
        debug = self.get_variable('DEBUG')

        args = [
            "sam", "package",
            "--template-file", sam_template_file,
            "--s3-bucket", s3_bucket,
            "--output-template-file", cloudformation_template_file
        ]

        if debug == True:
            args.append('--debug')

        result = subprocess.run(args,
                                check=False,
                                text=True,
                                encoding="utf-8",
                                stdout=sys.stdout,
                                stderr=sys.stderr)

        if result.returncode != 0:
            self.fail(f'{MSG_AWS_SAM_PACKAGE_FAILED}')

        # set TEMPLATE to packaged_cloudformation template ready for deploy
        self.variables['TEMPLATE'] = cloudformation_template_file
        self.env['TEMPLATE'] = cloudformation_template_file

        logger.info(f'Packaged application uploaded to S3 bucket {s3_bucket} and generated CloudFormation template written to {cloudformation_template_file}.')

    def run(self):
        command = self.get_variable('COMMAND')

        if command == "package-only" or command == "all":
            # build artifacts packaged_template and upload zipped app to AWS S3
            self.schema = package_schema
            self.variables = self.validate()
            self.package()

        if command == "deploy-only" or command == "all":
            # deploy App with aws-cloudformation-deploy using packaged.yml linked to S3 with App code
            self.schema = cloudformation_schema
            variables = self.validate()
            self.variables.update(variables)

            logger.info(f'Start deploy...')
            super().run()


if __name__ == '__main__':
    metadata = yaml.safe_load(open('/usr/bin/pipe.yml', 'r'))
    pipe = SamDeployPipe(schema=common_schema, pipe_metadata=metadata, check_for_newer_version=True)
    pipe.run()
