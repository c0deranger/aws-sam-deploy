from contextlib import contextmanager
from copy import copy
import importlib
import io
import os
import sys
from unittest import mock, TestCase
import yaml

import botocore.session
from botocore.stub import Stubber, ANY
from pipe import main


@contextmanager
def capture_output():
    standard_out = sys.stdout
    try:
        stdout = io.StringIO()
        sys.stdout = stdout
        yield stdout
    finally:
        sys.stdout = standard_out
        sys.stdout.flush()


class SamDeployTestCase(TestCase):

    def setUp(self):
        self.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    def tearDown(self):
        sys.path = self.sys_path

    @mock.patch.dict(os.environ, {'COMMAND': 'package-only', 'S3_BUCKET': 'test',
                                  'BITBUCKET_PIPE_STORAGE_DIR': os.getcwd(),
                                  'AWS_ACCESS_KEY_ID': 'akiafkae', 'AWS_SECRET_ACCESS_KEY': 'secretkey',
                                  'AWS_DEFAULT_REGION': 'test-region'})
    @mock.patch('subprocess.run')
    def test_aws_sam_deploy_package_only(self, mock_subprocess):
        mock_subprocess.return_value = mock.Mock(returncode=0)
        current_pipe_module = importlib.import_module('sam_pipe.core')

        with open(os.path.join(os.getcwd(), 'pipe.yml')) as f:
                metadata = yaml.safe_load(f.read())
        sam_deploy_pipe = current_pipe_module.SamDeployPipe(schema=current_pipe_module.common_schema,
                                                            pipe_metadata=metadata, check_for_newer_version=True)
        sam_deploy_pipe.run()

        mock_subprocess.assert_called_once_with(['sam', 'package', '--template-file', 'template.yaml', '--s3-bucket',
                                                 'test',
                                                 '--output-template-file', os.path.join(os.getcwd(), 'packaged.yml')],
                                                check=False, encoding=mock.ANY, stderr=mock.ANY, stdout=mock.ANY,
                                                text=True)

    @mock.patch.object(main.CloudformationDeployPipe, 'get_client')
    @mock.patch.dict(os.environ, {'COMMAND': 'deploy-only', 'STACK_NAME': 'test',
                                  'TEMPLATE': 'https://fake-bubucket.s3.amazonaws.com/test-packaged.yaml',
                                  'WAIT': 'True', 'WAIT_INTERVAL': '1',
                                  'AWS_ACCESS_KEY_ID': 'akiafkae', 'AWS_SECRET_ACCESS_KEY': 'secretkey',
                                  'AWS_DEFAULT_REGION': 'test-region',
                                  'BITBUCKET_COMMIT': 'abecdf',
                                  'BITBUCKET_BUILD_NUMBER': '1',
                                  'BITBUCKET_REPO_FULL_NAME': 'fake/fake'})
    def test_aws_sam_deploy_deploy_only(self, mock_cloudformation):
        cloudformation = botocore.session.get_session().create_client('cloudformation')
        mock_cloudformation.return_value = cloudformation
        stubber = Stubber(cloudformation)
        stubber.add_response('validate_template', {},
                             expected_params={
                                 'TemplateURL': 'https://fake-bubucket.s3.amazonaws.com/test-packaged.yaml'})
        stubber.add_response('update_stack',
                             {'StackId': 'ec1f24a'},
                             expected_params={
                                 'TemplateURL': 'https://fake-bubucket.s3.amazonaws.com/test-packaged.yaml',
                                 'StackName': 'test', 'Capabilities': [],
                                 'Parameters': ANY,
                                 'Tags': ANY}
                             )
        stubber.add_response('describe_stacks', {'Stacks': [{'StackId': 'ec1f24a', 'StackName': 'test',
                                                             'StackStatus': 'UPDATE_COMPLETE',
                                                             'CreationTime': '00:00:00'}]},
                             expected_params={'StackName': 'test'})

        stubber.add_response('describe_stack_events', {
            'StackEvents': []
        }, expected_params={'StackName': 'ec1f24a'})
        stubber.activate()

        current_pipe_module = importlib.import_module('sam_pipe.core')

        with open(os.path.join(os.getcwd(), 'pipe.yml')) as f:
            metadata = yaml.safe_load(f.read())
        sam_deploy_pipe = current_pipe_module.SamDeployPipe(
            schema=current_pipe_module.common_schema, pipe_metadata=metadata, check_for_newer_version=True)
        with capture_output() as out:
            sam_deploy_pipe.run()

        self.assertRegex(out.getvalue(), rf'✔ Successfully updated the test stack')
        self.assertIn(rf'console.aws.amazon.com/cloudformation/home?region=test-region#/stack/detail?stackId=ec1f24a',
                      out.getvalue())

    @mock.patch.dict(os.environ, {'S3_BUCKET': 'test',
                                  'BITBUCKET_PIPE_STORAGE_DIR': os.getcwd(),
                                  'STACK_NAME': 'test',
                                  'AWS_ACCESS_KEY_ID': 'akiafkae', 'AWS_SECRET_ACCESS_KEY': 'secretkey',
                                  'AWS_DEFAULT_REGION': 'test-region'})
    @mock.patch.object(main.CloudformationDeployPipe, 'run')
    @mock.patch('subprocess.run')
    def test_aws_sam_deploy_all(self, mock_subprocess, mock_cloudformation):
        mock_subprocess.return_value = mock.Mock(returncode=0)
        current_pipe_module = importlib.import_module('sam_pipe.core')

        with open(os.path.join(os.getcwd(), 'pipe.yml')) as f:
            metadata = yaml.safe_load(f.read())
        sam_deploy_pipe = current_pipe_module.SamDeployPipe(
            schema=current_pipe_module.common_schema, pipe_metadata=metadata, check_for_newer_version=True)
        sam_deploy_pipe.run()

        mock_subprocess.assert_called_once_with(['sam', 'package', '--template-file', 'template.yaml', '--s3-bucket',
                                                 'test',
                                                 '--output-template-file', os.path.join(os.getcwd(), 'packaged.yml')],
                                                check=False, encoding=mock.ANY, stderr=mock.ANY, stdout=mock.ANY,
                                                text=True)
        mock_cloudformation.assert_called_once()
        self.assertEqual(sam_deploy_pipe.get_variable('STACK_NAME'), 'test')
        self.assertEqual(sam_deploy_pipe.get_variable('S3_BUCKET'), 'test')
        self.assertEqual(sam_deploy_pipe.get_variable('TEMPLATE'), os.path.join(os.getcwd(), 'packaged.yml'))
        self.assertEqual(sam_deploy_pipe.get_variable('CFN_TEMPLATE'), 'packaged.yml')
